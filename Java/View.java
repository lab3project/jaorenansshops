import javax.swing.JOptionPane;

public class View {

	public static String lerString(String msg){
		return JOptionPane.showInputDialog(msg);
	}
	
	public static int lerInteiro(String msg){
		int valor;
		valor = Integer.parseInt(JOptionPane.showInputDialog(msg));
		return valor;
	}
	
	public static double lerDouble(String msg){
		 double valor;
		 valor = Double.parseDouble(JOptionPane.showInputDialog(msg));
		 return valor;
	}
	
	public static void mostraMsg(String msg){
		JOptionPane.showMessageDialog(null,msg);
	}
}
