
public class Usuario {
		
	private String Login;
	private String senha;
	private String email;
	
	Usuario(String login,String senha,String email){
		setEmail(email);
		setLogin(login);
		setSenha(senha);
	}

	public static boolean verificaLogin(String login,String senha, Usuario user){
		
		boolean retorno = false;
		
		if(login.equals(user.getLogin()))
			retorno = true;
		if(senha.equals(user.getSenha()))
			retorno = true;
		
		return retorno;
	}
	
	
	public String getLogin() {
		return Login;
	}

	public void setLogin(String login) {
		if(login.isEmpty())
			throw new IllegalArgumentException("*Campo Obrigatorio: login invalido.");
		else
			Login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		if(senha.isEmpty())
			throw new IllegalArgumentException("*Campo Obrigatorio: senha invalido.");
		else
			this.senha = senha;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		if(email.isEmpty())
			throw new IllegalArgumentException("*Campo Obrigatorio: email invalido.");
		else
			this.email = email;
	}
}
