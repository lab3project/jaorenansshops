
public class Produto {
	
	private String nome;
	private int qtd;
	private double preco;

	Produto(String nome,int qtd,double preco){
		setNome(nome);
		setPreco(preco);
		setQtd(qtd);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		if(nome.isEmpty())
			throw new IllegalArgumentException("*Campo Obrigatorio: Nome do produto não pode ser vazio.");
		else
			this.nome = nome;
	}

	public int getQtd() {
		return qtd;
	}

	public void setQtd(int qtd) {
		if(qtd<0)
			throw new IllegalArgumentException("*Campo Invalido: quantidade invalida.");
		else
			this.qtd = qtd;
		
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		if(preco<0)
			throw new IllegalArgumentException("*Campo invalido: preço invalido.");
		else
			this.preco = preco;
	}
	
	
}
