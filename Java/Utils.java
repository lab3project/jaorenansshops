
public class Utils {

	public static Usuario createUsuario(){
		Usuario currentUsuario = new Usuario(View.lerString("Informe o login:"),View.lerString("Informe a senha:"),View.lerString("Informe o email para confirmação do cadastro:"));
		return currentUsuario;
	}

	public static Produto createProduto(){
		Produto currentProduto = new Produto(View.lerString("Informe o nome do produto:"),View.lerInteiro("Informe a quantidade:"),View.lerDouble("Informe o preço:"));
		return currentProduto;
	}
	
}
